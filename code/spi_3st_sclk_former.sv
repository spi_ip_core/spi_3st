/*******************************************************************************
    Описание:
                Модуль формирования сигнала тактовой частоты SPI
    Комментарии:
                
    License:
        This library is free software; you can redistribute it and/or           
        modify it under the terms of the GNU Lesser General Public              
        License as published by the Free Software Foundation; either            
        version 2.1 of the License, or (at your option) any later version.      
                                                                                
        This library is distributed in the hope that it will be useful,         
        but WITHOUT ANY WARRANTY; without even the implied warranty of          
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       
        Lesser General Public License for more details.                         
                                                                                
        You should have received a copy of the GNU Lesser General Public        
        License along with this library; if not, write to the Free Software     
        Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111*1307 USA
                                                                                
                ***  GNU LESSER GENERAL PUBLIC LICENSE  ***                     
                  from http://www.gnu.org/licenses/lgpl.txt                     

    Разработчик:
        Bolshevik <bolshevikov.igor@gmail.com>
*******************************************************************************/

/*******************************************************************************
 * При появлении строба "run" запускаем автомат состояний управляющий этапами
 * формирования sclk.
 * - Вначале ожидаем некоторое время для установки сигнала ss_n
 *   (счетчик ss_delay_cnt);
 * - запускаем формирователь импульсов sclk:
 *     время каждого полупериода sclk определяется счетчиком period_cnt;
 *     количество импульсов указывается во входном аргументе trans_len;
 *     как только отсчитали trans_len * 2 полупериодов - переходим в следующее
 *     состояние;
 * - в этом состоянии удерживаем линию ss_n для соответствия таймингу удержания
 *   линии ss_n;
 ******************************************************************************/

// synthesis translate_off
timeunit      1ps;
timeprecision 1ps;
// synthesis translate_on

import aux_functions::*;
import custom_types_pkg::*;
import spi_3st_pkg::*;

module spi_3st_sclk_former
    (
        /* 
         * сигналы тактирования и сброса
         */
        input  wire                             clk,
        input  wire                             reset_n,

        /*
         * сигналы управления
         */
        /* слово делителя частоты */
        input  wire [lindex(DIVR_WIDTH):0]      divider,
        /* 
         * слово длины сообщения в транзакции (для транзакции с двунаправленной
         * передачей это длина приемного + передаваемого сообщений)
         */
        input  wire [lindex(PACK_LEN_WIDTH):0]  trans_len,
        /* флаг выбора начального сотояния линии клока */
        input  wire                             cpol_hi,
        /* флаг запуска генератора sclk */
        input  wire                             run,
        /* флаг разрешения прервания */
        input  wire                             ie,
        
        /*
         * формируемые сигналы
         */
        /* выходной вектор счетчика тактов в текущей транзакции */
        output wire  [lindex(PACK_LEN_WIDTH):0] tick_index,
        /* строб обозначающий событие появления переднего фронта sclk */
        output logic                            rise_strob,
        /* строб обозначающий событие появления заднего фронта sclk */
        output logic                            fall_strob,
        /* выход сформированного тактового сигнала */
        output wire                             sclk,
        /* сигнал выбора подчиненного устройства */
        output wire                             ss_n,
        /* прерывание по факту конца посылки */
        output logic                            irq,
        /* вспомогательный сигнал-маркер начала транзакции */
        output wire                             start_event
    );

    /***************************************************************************
     * Типы данных, сигналы, константы
     **************************************************************************/
    logic                               trans_ended;
    logic [lindex(DIVR_WIDTH - 1):0]    period_cnt;
    logic [lindex(PACK_LEN_WIDTH):0]    tick_index_2x;
    /* отлавливаем момент когда счетчик будет равен 0 */
    wire period_cnt_zero = !(period_cnt);

    /***************************************************************************
     * Счетчик предустановки/удержания линий ss_n в начале и конце каждой
     * транзакции
     **************************************************************************/
    logic [lindex(SS_DELAY_WIDTH):0]    ss_delay_cnt;
    logic                               hold_ss_delay_cnt;
    always_ff @(posedge clk or negedge reset_n) begin : proc_ss_delay
        if(~reset_n) begin
            ss_delay_cnt    <= '0;
        end else begin
            if(hold_ss_delay_cnt) begin
                ss_delay_cnt <= SS_DELAY_LEN;
            end else begin
                ss_delay_cnt--;
            end
        end
    end

    /***************************************************************************
     * Автомат состояний управляющий порядком включения/выключения счетчиков
     * предустановки/удержания, начала транзакции
     **************************************************************************/
    ss_ctrl_t   trans_state;
    logic       en_trans;
    logic       en_trans_r;
    wire        start_trans = !en_trans_r & en_trans;
    assign      start_event = start_trans;
    always_ff @(posedge clk or negedge reset_n) begin : proc_state_mach
        if(~reset_n) begin
            trans_state         <= DIS;
            hold_ss_delay_cnt   <= '0;
            en_trans            <= '0;
            en_trans_r          <= '0;
        end else begin
             case (trans_state)
                DIS: begin
                    hold_ss_delay_cnt   <= '1;
                    en_trans            <= '0;
                    if(run) begin
                        trans_state         <= PRESET_SS_TIMER;
                    end
                end
                PRESET_SS_TIMER: begin
                    hold_ss_delay_cnt   <= '0;
                    en_trans            <= '0;
                    if(!(|ss_delay_cnt)) begin
                        trans_state         <= DATA_TRANSACT;
                    end
                end
                DATA_TRANSACT: begin
                    hold_ss_delay_cnt   <= '1;
                    en_trans            <= '1;
                    if(!(|tick_index_2x)) begin
                        trans_state         <= HOLD_SS_TIMER;
                    end
                end
                HOLD_SS_TIMER: begin
                    hold_ss_delay_cnt   <= '0;
                    en_trans            <= '0;
                    if(!(|ss_delay_cnt)) begin
                        trans_state         <= DIS;
                    end
                end
             endcase

            en_trans_r <= en_trans;
        end
    end

    /***************************************************************************
     * Управление сигналов выбора ведомого устройства
     * Внимание!
     *      Алгоритм управления ss_n предполагает что код состояния DIS == 0
     *      (иначе пришлось бы ставить дополнительную схему сравнения)
     **************************************************************************/
    assign ss_n = !(|trans_state);

    /***************************************************************************
     * Цикл счетчика формирующего длину полупериода сигнала sclk
     **************************************************************************/
    always_ff @(posedge clk or negedge reset_n) begin : proc_period_cnt
        if(~reset_n) begin
            period_cnt <= '1;
        end else begin
            if(start_trans || period_cnt_zero) begin
                period_cnt      <= divider;
            end else if(!trans_ended) begin
                period_cnt--;
            end
        end
    end

    /***************************************************************************
     * Цикл формирования стробов событий появляения фронта (любого) sclk
     **************************************************************************/
    logic front_r;
    always_ff @(posedge clk or negedge reset_n) begin : proc_search_front
        if(~reset_n) begin
            front_r             <= '0;
        end else begin
            if(ss_n) begin
                front_r         <= cpol_hi;
            end else if(period_cnt_zero) begin
                front_r         <= !front_r;
            end
        end
    end

    /***************************************************************************
     * Цикл формирования стробов событий переднего/заднего фронта sclk
     **************************************************************************/
    always_ff @(posedge clk or negedge reset_n) begin : proc_gen_front_strobs
        if(~reset_n) begin
            rise_strob <= '0;
            fall_strob <= '0;
        end else begin
            if(period_cnt_zero) begin
                rise_strob <= !front_r;
                fall_strob <= front_r;
            end else begin
                rise_strob <= '0;
                fall_strob <= '0;
            end
        end
    end

    /***************************************************************************
     * Счетчик периодов sclk
     * 
     * При загрузки длины посылки в счетчик умножаем на 2 длину, т.к. счетчик
     * period_cnt_zero отсчитывает полупериоды
     **************************************************************************/
    always_ff @(posedge clk or negedge reset_n) begin : proc_tick_index_2x
        if(~reset_n) begin
            tick_index_2x       <= trans_len;
        end else begin
            if(start_trans || trans_ended) begin
                tick_index_2x   <= trans_len << 1;
            end else if(period_cnt_zero) begin
                tick_index_2x--;
            end else begin
                tick_index_2x   <= tick_index_2x;
            end
        end
    end

    always_ff @(posedge clk or negedge reset_n) begin : proc_trans_ended_ctrl
        if(~reset_n) begin
            trans_ended <= '1;
        end else begin
            if(start_trans) begin
                trans_ended     <= '0;
            end else if(!(|tick_index_2x)) begin
                trans_ended     <= '1;
            end
        end
    end

    /***************************************************************************
     * Делитель индекса передаваемого бита на 2.
     * Необходим т.к. внутренний счетчик считает каждые пол периода.
     **************************************************************************/
    assign tick_index = tick_index_2x >> 1;
    
    /* назначим источник sclk */
    assign sclk = front_r;

    /***************************************************************************
     * Формирователь прерывания в конце транзакции
     **************************************************************************/
    wire end_trans = en_trans_r & !en_trans;
    always_ff @(posedge clk or negedge reset_n) begin : proc_irq_former
        if(~reset_n) begin
            irq <= '0;
        end else begin
            if(end_trans & ie) begin
                irq     <= '1;
            end else if(!ie) begin
                irq     <= '0;
            end
        end
    end

endmodule // spi_3st_sclk_former